﻿namespace ClassLibraryCalculation
{
    public class GetExpiryDate
    {
        public DateTime ExpiryDate(DateTime purchaseDate)
        {
            // Calculate expiry date based on purchase date
            DateTime expiryDate;
            if (purchaseDate.Day >= 1 && purchaseDate.Day <= 10)
            {
                expiryDate = new DateTime(purchaseDate.Year, purchaseDate.Month, DateTime.DaysInMonth(purchaseDate.Year, purchaseDate.Month));
            }
            else if (purchaseDate.Day >= 11 && purchaseDate.Day <= 20)
            {
                expiryDate = new DateTime(purchaseDate.Year, purchaseDate.Month, DateTime.DaysInMonth(purchaseDate.Year, purchaseDate.Month)).AddDays(10);
            }
            else // For days 21 and onwards
            {
                expiryDate = new DateTime(purchaseDate.Year, purchaseDate.Month, DateTime.DaysInMonth(purchaseDate.Year, purchaseDate.Month)).AddMonths(1);
            }

            return expiryDate;
        }
    }
}
