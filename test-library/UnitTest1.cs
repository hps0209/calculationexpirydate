using ClassLibraryCalculation;

namespace test_library
{
    public class UnitTest1
    {
        [Fact]
        public void GetExpiryDateLastDayofMonth()
        {
            GetExpiryDate obj = new GetExpiryDate();
            DateTime purchaseDate = new DateTime(2024, 1, 9);
            DateTime expiryDate = new DateTime(2024,01,31);
            Assert.Equal(expiryDate, obj.ExpiryDate(purchaseDate));
        }

        [Fact]
        public void GetExpiryNextMonthDate()
        {
            GetExpiryDate obj = new GetExpiryDate();
            DateTime purchaseDate = new DateTime(2024, 1, 15);
            DateTime expiryDate = new DateTime(2024, 02, 10);
            Assert.Equal(expiryDate, obj.ExpiryDate(purchaseDate));
        }

        [Fact]
        public void GetExpiryDateLastDayofNextMonth()
        {
            GetExpiryDate obj = new GetExpiryDate();
            DateTime purchaseDate = new DateTime(2024, 1, 22);
            DateTime expiryDate = new DateTime(2024, 02, 29);
            Assert.Equal(expiryDate, obj.ExpiryDate(purchaseDate));
        }
    }
}