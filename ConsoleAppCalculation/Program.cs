﻿using ClassLibraryCalculation;

Console.Write("Enter the purchase date (yyyy-MM-dd format): ");

if (DateTime.TryParse(Console.ReadLine(), out DateTime purchaseDate))
{
    GetExpiryDate obj = new GetExpiryDate();
    DateTime expiryDate = obj.ExpiryDate(purchaseDate);
    Console.WriteLine("Expiry Date: " + expiryDate.ToString("yyyy-MM-dd"));
}
else
{
    Console.WriteLine("Invalid date format!");
}